<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::post('/categories/create', 'HomeController@save')->name('categories.save');
Route::get('/categories/create/{slug?}', 'HomeController@create');
Route::get('/categories/', 'HomeController@categoryIndex');
Route::get('/categories/', 'HomeController@categoryIndex')->name('categories.index');
Route::get('/categories/delete/{id}', 'HomeController@categoryDelete')->name('categories.delete');
Route::get('/categories/{slug}', 'HomeController@category')->name('categories.show');

