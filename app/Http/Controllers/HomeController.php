<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use models\Categories;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function category($slug)
    {
        $category = Category::where(['slug' => $slug])->first();
        return view('home', [
            'category' => $category
        ]);
    }

    public function create(Request $request, $slug = null) {
        $category = null;
        if ($slug) {
            $category = Category::where(['slug' => $slug])->first();
        }

        return view('create', [
            'category' => $category
        ]);
    }

    public function categoryIndex() {

            $categories = Category::all();


        return view('home', [
            'categories' => $categories
        ]);
    }
    public function categoryDelete($id) {

        $categories = Category::find($id);
        $categories->delete();

        return redirect(route('categories.index'));
    }


    public function save(Request $request) {
        $name = $request->get('name');
        $slug = $request->get('slug');
        $id = $request->get('id');

        if($id === null) {
            $category = new Category();
        } else {
            $category = Category::find($id);
        }

        $category->name = $name;
        $category->slug = $slug;
        $category->save();
        return redirect(route('categories.show', ['slug' => $slug]));
    }





}
