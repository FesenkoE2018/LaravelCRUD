<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="{{ route('categories.save') }}" method="POST">

    {{ csrf_field() }}

        name: <input type="text" name="name" value="@if ($category) {{ $category->name }} @endif"></br>
        slug: <input type="text" name="slug" value="@if ($category) {{ $category->slug }} @endif"></br>
        <input type="hidden" name="id" value="@if ($category) {{ $category->id }} @endif">
        <input type="submit" value="create">

</form>
</body>
</html>